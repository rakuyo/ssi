#!/usr/bin/perl -w
use IO::Socket::INET;
use warnings;

my $name = $ARGV[0] | shift;

## Server Side Information ##
my $server = '172.16.1.59';
my $port = '5000';
my $proto = 'tcp';

my $socket = IO::Socket::INET->new(
	PeerAddr => $server,
	PeerPort => $port,
	Proto    => $proto ) || die "[!] Cannot connect to $server:$port\n";

print "Establishing Connection to $server:$port\n";
$socket->autoflush(1);

#Send File
sendZIP();

## Close Socket Server Connection ##
close $socket || die "\n[!] Cannot close socket connection..\n";
print "\nZIP succesfuly sent to Server\n";

sub sendZIP {
	my $filename = $name . '.zip';
	open (FILE, '<', $filename) || die "[!] Cannot Open File $filename\n";
	binmode FILE;

	## Send ZIP File ##
	print "Sending ZIP File.\n";
	while (<FILE>) {
		print '.';
	 	print $socket $_;
	}

	## Close ZIP file ##
	close FILE || die "\n[!] Cannot Close File ZIP File\n";
}
