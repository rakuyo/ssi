require('generate.pl');
my $STORECODE = $ARGV[0] | shift;

## Generate Checksum of ZIP ##
my $file = generate($STORECODE);
my @checksum_script = ('perl', '/usr/local/imperium/retail/jobqueue/bin/client/send_checksum.pl', $file);
my @zip_script = ('perl', '/usr/local/imperium/retail/jobqueue/bin/client/send_zip.pl', $file);

## Send Checksum first before the Actual File ##
#system('perl', '/usr/local/imperium/retail/jobqueue/bin/client/send_checksum.pl');
system @checksum_script;
system @zip_script;



