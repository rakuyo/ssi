#!/usr/bin/perl -w
use IO::Socket::INET;
use warnings;

my $name = $ARGV[0] | shift;

## Server Side Information ##
my $server = '172.16.1.59';
my $port = '5000';
my $proto = 'tcp';

my $socket = IO::Socket::INET->new(
	PeerAddr => $server,
	PeerPort => $port,
	Proto    => $proto ) || die "[!] Cannot connect to $server:$port\n";

print "Establishing Connection to $server:$port\n";
$socket->autoflush(1);

#Send File
sendMD5();

## Close Socket Server Connection ##
close $socket || die "\n[!] Cannot close socket connection..\n";
print "\nMD5 succesfuly sent to Server\n";

sub sendMD5 {
	my $filename = $name . '.md5';
	open (FILE, '<', $filename) || die "[!] Cannot Open File $filename\n";
	binmode FILE;

	## Send MD5 File ##
	print "Sending MD5 File.\n";
	while (<FILE>) {
		print '.';
	  print $socket $_;  
	}

	## Close MD5 file ##
	close FILE || die "\n[!] Cannot Close File MD5 File\n";
}
