#!/usr/bin/perl -w
use warnings;
use POSIX qw(strftime);
use Digest::MD5 qw(md5_hex);
use IO::Compress::Zip qw(zip $ZipError);

my $dir = '/home/www/poll';
my $file_ext = 'POL';

sub generate {
  chdir $dir;

  $poll = $file_ext . $_[0];
  
  ## Open Zip file ##
  open (FILE_ZIP, '<', $poll . '.zip') || die $!;

  ## Create MD5 ##
  my $cksum = md5_hex(<FILE_ZIP>);
  open (FILE_MD5 , '>', $poll . '.md5') || die $!;
  print FILE_MD5 $cksum;

  ## Close Files##
  #close FILE_STORE || die $!;
  close FILE_ZIP || die $!;
  close FILE_MD5 || die $!;

  return $poll;
}

1;
