-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (i686)
--
-- Host: 172.31.1.3    Database: ssi
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `department_code` char(3) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `department_code` (`department_code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'F&F','1',NULL,1),(2,'Aeropostale','2',NULL,1),(3,'P.S Aeropostale','3',NULL,1),(4,'Desigual','4',NULL,1),(5,'Cache Cache','5',NULL,1),(6,'Muji','6',NULL,1),(7,'Pottery Barn','7',NULL,1),(8,'Aerosoles','8',NULL,1),(9,'Hackett','9',NULL,1),(10,'Dune','10',NULL,1),(14,'RR','11','r\'s',1),(15,'2','12','2',1),(16,'3','13','1',1),(17,'1','14','1',1);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eod_uploads`
--

DROP TABLE IF EXISTS `eod_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eod_uploads` (
  `id` char(32) NOT NULL,
  `store_id` int(11) NOT NULL,
  `poll_path` varchar(255) NOT NULL,
  `poll_checksum` char(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `attempts` int(2) NOT NULL DEFAULT '1',
  `error_message_id` int(11) NOT NULL,
  `sales_date` date NOT NULL,
  `total_sales` decimal(20,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`store_id`,`sales_date`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `error_message_id` (`error_message_id`),
  CONSTRAINT `eod_uploads_ibfk_2` FOREIGN KEY (`error_message_id`) REFERENCES `error_messages` (`id`),
  CONSTRAINT `eod_uploads_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eod_uploads`
--

LOCK TABLES `eod_uploads` WRITE;
/*!40000 ALTER TABLE `eod_uploads` DISABLE KEYS */;
INSERT INTO `eod_uploads` VALUES ('1',1,'/the/path/to/heaven','poll sum',1,1,0,'2015-02-16',500.00,'2015-02-15 16:00:00','0000-00-00 00:00:00',0,0),('10',7,'asdasd','asdasdsa',1,1,0,'2015-02-19',500.00,'2015-02-19 16:00:00','2015-02-20 12:06:46',0,0),('11',8,'asdasd','aasdaaaaaasd',0,1,0,'2015-02-19',0.00,'0000-00-00 00:00:00','2015-02-20 12:06:56',0,0),('2',2,'/path/up/stairs','check checl',1,1,0,'2015-02-16',550.00,'2015-02-15 16:00:00','0000-00-00 00:00:00',0,0),('21e3ed38c7ba57fdf264b438dc0d0327',15,'/home/www/poll/20140306/fairview/00335','044bdfed98a279fe4be1b5ee00b57627',1,19,0,'2015-02-16',8387.50,'2015-02-17 16:00:00','2015-02-25 22:38:13',0,1),('3',7,'any/path','sumcheck',0,1,0,'2015-02-16',0.00,'2015-02-15 16:00:00','2015-02-25 20:00:15',2,1),('4',6,'/random/path','check.sum.this',0,1,0,'2015-02-16',0.00,'2015-03-15 16:00:00','2015-02-23 07:06:32',2,2),('7',10,'/new/data','/x/xx/.',1,1,0,'2015-02-16',111.00,'2015-02-18 16:00:00','2015-02-20 11:49:12',0,0),('c3699ac0419367f100b06d7fe489abde',7,'/home/www/poll/20150207/fairview/00335','b67613775bfa0f1d4682bd247c134c14',1,2,0,'2015-02-07',11698200.00,'2015-02-23 14:18:56','2015-02-23 14:25:13',0,1),('e8eca55098c88146531ca9d986af44e5',14,'/home/www/poll/20150220/fairview/00335','',0,15,0,'2015-02-16',0.00,'2015-02-20 03:53:48','2015-02-25 04:15:14',0,1);
/*!40000 ALTER TABLE `eod_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `error_messages`
--

DROP TABLE IF EXISTS `error_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `error_messages`
--

LOCK TABLES `error_messages` WRITE;
/*!40000 ALTER TABLE `error_messages` DISABLE KEYS */;
INSERT INTO `error_messages` VALUES (0,'unknown error',NULL,1);
/*!40000 ALTER TABLE `error_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jda_versions`
--

DROP TABLE IF EXISTS `jda_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jda_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jda_versions`
--

LOCK TABLES `jda_versions` WRITE;
/*!40000 ALTER TABLE `jda_versions` DISABLE KEYS */;
INSERT INTO `jda_versions` VALUES (0,'4r4',NULL,1);
/*!40000 ALTER TABLE `jda_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jdas`
--

DROP TABLE IF EXISTS `jdas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jdas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `jda_version_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`),
  KEY `jda_version_id` (`jda_version_id`),
  CONSTRAINT `jdas_ibfk_1` FOREIGN KEY (`jda_version_id`) REFERENCES `jda_versions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jdas`
--

LOCK TABLES `jdas` WRITE;
/*!40000 ALTER TABLE `jdas` DISABLE KEYS */;
INSERT INTO `jdas` VALUES (1,NULL,'172.16.1.1',0,1),(2,NULL,'172.16.1.4',0,1),(3,NULL,'172.31.1.2',0,1),(4,NULL,'172.31.2.3',0,1),(5,'new','172.31.11',0,1);
/*!40000 ALTER TABLE `jdas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Century City Mall',NULL,1),(2,'SM North Edsa',NULL,1),(3,'Estancia',NULL,1),(4,'BHS',NULL,1),(5,'Fairview',NULL,1),(6,'Dasma',NULL,1),(7,'Cebu',NULL,1),(8,'Magnolia',NULL,1),(9,'Central Square',NULL,1),(10,'Mall of Asia',NULL,1),(11,'Rockwell',NULL,1),(12,'Fort Bonifacio',NULL,1),(13,'Greenbelt5',NULL,1),(14,'qawewqeqwe','12321',1),(15,'12321321313','123213',1);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll04s`
--

DROP TABLE IF EXISTS `poll04s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll04s` (
  `id` int(11) NOT NULL,
  `eod_upload_id` char(32) NOT NULL,
  `transaction_type` char(2) NOT NULL,
  `transaction_date` char(6) NOT NULL,
  `transaction_time` char(6) NOT NULL,
  `cashier_id` char(5) NOT NULL,
  `register_number` char(2) NOT NULL,
  `transaction_number` int(5) NOT NULL,
  `tender_sequence_number` int(5) NOT NULL,
  `tender_fund_code` char(2) NOT NULL,
  `tender_amount` decimal(12,2) NOT NULL,
  `credit_card_number` char(19) NOT NULL,
  `expiration_date` char(4) NOT NULL,
  `credit_authorization_code` char(6) NOT NULL,
  `reference_type` char(1) NOT NULL,
  `misc_referende_info` varchar(16) NOT NULL,
  `customer_number` char(8) NOT NULL,
  `status` varchar(50) NOT NULL,
  `employee_number` char(5) NOT NULL,
  `customer_postal_code` varchar(9) NOT NULL,
  `non-keyboard_input_device_indicator` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll04s`
--

LOCK TABLES `poll04s` WRITE;
/*!40000 ALTER TABLE `poll04s` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll04s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll05s`
--

DROP TABLE IF EXISTS `poll05s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll05s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eod_upload_id` char(32) NOT NULL,
  `transaction_type` tinyint(1) NOT NULL,
  `transaction_item_type` tinyint(1) NOT NULL,
  `transaction_date` char(6) NOT NULL,
  `transaction_time` char(6) NOT NULL,
  `cashier_id` char(5) NOT NULL,
  `register_number` char(2) NOT NULL,
  `transaction_number` char(5) NOT NULL,
  `item_sequence_number` char(5) NOT NULL,
  `sku` char(9) NOT NULL,
  `item_quantity` decimal(7,2) NOT NULL,
  `item_original_price` decimal(9,2) NOT NULL,
  `promotional_sale_code` char(4) NOT NULL,
  `promotional_sale_price` decimal(9,2) NOT NULL,
  `item_override_price` decimal(9,2) NOT NULL,
  `discount_type` char(1) NOT NULL,
  `discount_amount` decimal(9,2) NOT NULL,
  `item_selling_price` decimal(9,2) NOT NULL,
  `item_extended_selling_price` decimal(12,2) NOT NULL,
  `tax1_indicator` char(1) NOT NULL,
  `tax2_indicator` char(1) NOT NULL,
  `reference_type` char(1) NOT NULL,
  `misc_information` char(1) NOT NULL,
  `item_universal_product_code` char(13) NOT NULL,
  `clerk` char(5) NOT NULL,
  `customer_name` varchar(35) NOT NULL,
  `customer_number` varchar(10) NOT NULL,
  `item_return_reason_code` char(4) NOT NULL,
  `original_salesperson` char(5) NOT NULL,
  `original_store` char(5) NOT NULL,
  `subtotal_discount_amount` decimal(9,2) NOT NULL,
  `item_discount_reason_code` char(2) NOT NULL,
  `transaction_discount_reason_code` char(2) NOT NULL,
  `non-keyboard_input_device_indicator` char(1) NOT NULL,
  `taxing_authority_code` char(6) NOT NULL,
  `taxing_rate_code` char(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eod_upload_id` (`eod_upload_id`),
  CONSTRAINT `poll05s_ibfk_1` FOREIGN KEY (`eod_upload_id`) REFERENCES `eod_uploads` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll05s`
--

LOCK TABLES `poll05s` WRITE;
/*!40000 ALTER TABLE `poll05s` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll05s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `jda_id` int(11) NOT NULL,
  `jda_path` varchar(255) NOT NULL,
  `code` char(5) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `opening_time` time DEFAULT NULL,
  `closing_time` time DEFAULT NULL,
  `assigned_to` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `ip` (`ip`),
  KEY `location_id` (`location_id`,`brand_id`,`jda_id`,`assigned_to`),
  KEY `jda_id` (`jda_id`),
  KEY `brand_id` (`brand_id`),
  KEY `assigned_to` (`assigned_to`),
  CONSTRAINT `stores_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  CONSTRAINT `stores_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `stores_ibfk_3` FOREIGN KEY (`jda_id`) REFERENCES `jdas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,1,1,1,'qdls\\MMFFSFLR\\90708','90708','172.16.88.193',NULL,NULL,1,1),(2,2,1,2,'qdls\\MMFFSFLR\\90707','90707','172.16.88.161',NULL,NULL,1,1),(3,3,2,1,'qdls\\MMAPSFLR\\01412','01412','172.16.90.1',NULL,NULL,1,1),(4,3,3,1,'qdls\\MMAPSFLR\\01413','01413','172.16.90.20',NULL,NULL,1,1),(5,4,4,2,'qdls\\MMDSSFLR\\00066','00066','172.16.169.104',NULL,NULL,1,1),(6,1,4,2,'qdls\\MMDSSFLR\\00086','00086','172.16.161.3',NULL,NULL,1,1),(7,5,5,1,'qdls\\MMB2BFLR\\00334','00335','172.16.134.50',NULL,NULL,1,1),(8,6,5,1,'qdls\\MMB2BFLR\\00340','00340','172.16.63.50',NULL,NULL,1,1),(9,7,6,1,'qdls\\MMMRRFLR\\00220','00220','172.16.180.10',NULL,NULL,1,1),(10,8,6,1,'qdls\\MMMRRFLR\\00776','00776','172.16.128.100',NULL,NULL,1,1),(11,9,7,2,'qdls\\MMPBSFLR\\00938','00938','172.16.136.9',NULL,NULL,1,1),(12,10,8,1,'qdls\\MM4R4FLR\\00531','00531','172.16.28.152',NULL,NULL,1,1),(13,11,9,1,'qdls\\MM4R4FLR\\00630','00630','172.16.15.142',NULL,NULL,1,1),(14,12,10,1,'qdls\\MM4R4FLR\\00928','00928','172.16.82.71','10:00:00','12:00:00',4,1),(15,13,10,1,'qdls\\MM4R4FLR\\00262','00262','172.16.99.145',NULL,NULL,1,1),(24,10,10,2,'randoms','90909','172.77777','10:00:00','10:00:00',1,1);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-27 11:34:46
