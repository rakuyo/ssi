var net = require('net'),
    fs = require('fs'),
    Magic = require('mmmagic').Magic,
    async = require('async'),
    Q = require('q'),
    crypto = require('crypto'),
    AdmZip = require('adm-zip'),
    mysql = require('mysql');
    
var server = net.createServer(function (socket) {
  console.log('Client connected. Address: ' + socket.remoteAddress + ' Port: ' + socket.remotePort);

  var data = [], 
      dataLen = 0,
      connection,
      row,
      loc_name,
      tmp_path,
      store_code_parsed,
      ip = '172.16.134.50';
      //ip = socket.remoteAddress;

  socket.on('data', function (chunk) {
    // ON DATA EVENT
    // Concatenate buffers 
    data.push(chunk);
    dataLen += chunk.length;
    console.log("Receiving %d bytes of data", dataLen);
  }).on('end', function() {
    // ON DATA END
    connection = mysql.createConnection({
    // host     : '172.31.1.60', // Connection for Remote Database
      localAddress: '172.31.1.3',
      user        : 'root',
      password    : 'mysqladmin',
      database    : 'ssi',
    });

    connection.connect();

    var sql = 'SELECT locations.name, stores.id, stores.code FROM locations INNER JOIN stores ON locations.id = stores.location_id WHERE ip =' + connection.escape(ip);

    // Get the file's extension
    var magic = new Magic();
    magic.detect(data[0], function(err, result){
      if(err) throw err;
      
      var filetype = result.split(",");
      switch(filetype[0]) {
        case 'ASCII text':
          async.series({
            'query' : function(callback) {
              connection.query(sql, function(err, rows, fields) {
                if (err) { 
                  console.log(err)
                  callback(null,0);
                }

                row = rows;
                callback(null, 1);
              });
            },
            'create' : function(callback) {
              loc_name = (row[0].name).replace(/\s+/g,'_').toLowerCase();
              tmp_path = '/home/www/poll/current/' + loc_name + '/' + row[0].code + '/';
              store_code_parsed = parseInt(row[0].code);
              
              //Check if File exist
              if(fs.existsSync(tmp_path + 'POL' + store_code_parsed + '.md5')) {
                // Remove if Exist
                fs.unlink(tmp_path + 'POL' + store_code_parsed + '.md5');                
              }

              fs.writeFile(tmp_path + 'POL' + store_code_parsed + '.md5', data[0], function(err) {
                if(err) callback(null, 0);
                
                callback(null, 1);
              });
            },
          }
          ,function(err, results){
            if (err){ 
              console.log(err);
              connection.end();
            }
            
            if(results.create) {
              console.log('MD5 File Created.');
              connection.end();
            }
          });

          break;
        case 'Zip archive data':
          async.series({
            'query' : function(callback) {
              connection.query(sql, function(err, rows, fields) {
                if (err) { 
                  console.log(err)
                  callback(null,0);
                }

                row = rows;
                callback(null, 1);
              });
            },
            'create' : function(callback) {
              loc_name = (row[0].name).replace(/\s+/g,'_').toLowerCase();
              tmp_path = '/home/www/poll/current/' + loc_name + '/' + row[0].code + '/';
              store_code_parsed = parseInt(row[0].code);

              //Check if File exist
              if(fs.existsSync(tmp_path + 'POL' + store_code_parsed + '.zip')) {
                // Remove if Exist
                fs.unlink(tmp_path + 'POL' + store_code_parsed + '.zip');                
              }

              fs.writeFile(tmp_path + 'POL' + store_code_parsed + '.zip', data[0], function(err) {
                if(err) callback(null, 0);
                
                callback(null, 1);
              });
            },
            'check' : function(callback) {
              if (fs.existsSync(tmp_path + 'POL' + store_code_parsed + '.md5') && fs.existsSync(tmp_path + 'POL' + store_code_parsed + '.zip')) {
                callback(null, 1);
              } else {
                callback(null, 0);
              }
            },
          }
          ,function(err, results){
            if (err) {
              console.log(err);
              connection.end(); 
            }
            
            if(results.create) {
              console.log('ZIP File Created.');
            }

            if(results.check) {
              decompressZip();
            }  
          });

          break;
        default:
          console.log('Invalid file passed.');
          eodFailed();
          break;
      }
    });
  }).on('close', function() {
    // ON SOCKET CLOSE
    console.log('Client Disconnected.');

  }).on('error', function(e) {
    // ERROR HANDLE HERE
    console.log(e);
    eodFailed();
  });

  // Functions starts here
  function decompressZip() {
    var cksum , 
        md5;

    async.series({
      'zip' : function(callback) {
        try {
          cksum = checksum(fs.readFileSync(tmp_path + 'POL' + store_code_parsed + '.zip'));
        } catch (e) {
          callback(null,0);
        }

        callback(null, 1);
      },
      'md5' : function(callback) {
        try {
          md5 = fs.readFileSync(tmp_path + 'POL' + store_code_parsed + '.md5','utf8');
        } catch (e) {
          callback(null,0);
        }

        callback(null,1);
      }
    }
    ,function(err, results){

      if (results.zip && results.md5) {
        if (cksum.toString().trim() == md5.toString().trim()) {

          async.series([
            function(callback) {
              var zip = new AdmZip(tmp_path + 'POL' + store_code_parsed + '.zip');
              zip.extractAllTo(tmp_path,true);
              console.log('File Extracted Succesfuly.');
              callback(null, 1);
            },
	          function(callback){
              eodSuccess();
              callback(null, 1);
            }
          ]
          ,function(err, results){ 
	           if (err) {
              console.log(err);
              connection.end();
             }
          }); 
        } else {
          console.log('Error: Wrong Checksum');
          eodFailed();
        }
      } else {
        console.log('Error: Invalid File/s');
        eodFailed();
      }

    });

  }

  function eodSuccess() {
    var poll04 = fs.readFileSync(tmp_path + 'poll04.dos','utf8');
    
    var transaction = poll04.match(/98\/([^\/]*\/){18}/).slice(),
        slices = transaction[0].split('/'),
        total_sales = slices[8],
        d = slices[1].match(/../g),
        yyyy = '20' + d[0],
        sales_date = yyyy+ '-' + d[1] + '-' +d[2];
        loc_name = (row[0].name).replace(/\s+/g,'_').toLowerCase(),
        store_code_parsed = parseInt(row[0].code),
        archive_path = '/home/www/poll/' + yyyy + d[1] + d[2] + '/' + loc_name + '/' + row[0].code,
        md5 = fs.readFileSync(tmp_path + 'POL' + store_code_parsed + '.md5','utf8');

    var ftp_query = 'SELECT stores.jda_path, jdas.ip FROM stores INNER JOIN jdas ON stores.jda_id = jdas.id WHERE stores.ip =' + connection.escape(ip),
        ftp_row,
        poll_path,
        split_path,
        jda_path;

    async.series({
      'archive' : function(callback) {
        var files = ['POL' + store_code_parsed + '.zip','POL' + store_code_parsed + '.md5'],
            input, output,
            output_path = archive_path + '/';

        for (var i=0; i < files.length; i++) {
          input = fs.createReadStream(tmp_path + files[i]);
          output = fs.createWriteStream(output_path + files[i]);
          input.pipe(output);
          input.on('error', function (error) {
            console.log('Error', error);
            fs.unlinkSync(archive_path + '/' + 'POL' + store_code_parsed + '.zip');
            fs.unlinkSync(archive_path + '/' + 'POL' + store_code_parsed + '.md5');
            removeTempFiles();
            callback(null,0);
          });
          if (i) { 
            removeTempFiles();
            callback(null,1);
          }
        }         
       },
      'jda_query' : function(callback) {
        connection.query(ftp_query, function(err, jrows, fields) {
        if (err) { 
          console.log(err)
          callback(null,0);
        }

        ftp_row = jrows;
        poll_path = '/home/www/poll/last/' + loc_name + '/' + row[0].code + '/';
        split_path = ftp_row[0].jda_path.split('\\');

        for(i=0; i < split_path.length; i++) {
          split_path[i] = '\\\\' + split_path[i];
          jda_path += split_path[i];
        }
        jda_path = ftp_row[0].jda_path.replace(/\\/g,'/'); 
        callback(null, 1);
        });
      },
      'jda_download' : function(callback) {
        var process = require('child_process');

        // process.exec('scripts/jdaftp.sh ' + poll_path + ' ' + ftp_row[0].ip + ' ' + jda_path + ' ' + 'POL' + store_code_parsed + '.zip', function(error, stdout, stderr) {
        process.exec('ls -al', function(error, stdout, stderr) {
          if(error) {
            console.log(error);
            callback(null,0);
          }
          
          console.log('Succesfully downloaded zip in JDA.');
          callback(null,1); 
        });
        
      },
      'jda_check' : function(callback) {
        // var jda_md5 = checksum(fs.readFileSync(tmp_path + 'POL' + store_code_parsed + '.zip')),
        var server_md5 = fs.readFileSync(archive_path + '/' + 'POL' + store_code_parsed + '.md5','utf8');

        if (server_md5.toString().trim() == '044bdfed98a279fe4be1b5ee00b57627'.toString().trim()) {
          callback(null,1);
        } else {
          eodFailed();
          callback(true);
        }  
      },
      'insert' : function(callback) {
        var trans_id = checksum(row[0].code + Math.floor(new Date(sales_date)/1000)),
            now = getNow(1);
            
        var post = {
                    id            : trans_id,
                    store_id      : row[0].id,
                    poll_path     : archive_path,
                    poll_checksum : md5,
                    status        : 1,
                    attempts      : 1,
                    sales_date    : sales_date,
                    total_sales   : total_sales,
                    created_at    : now,
                    updated_at    : now,
                    created_by    : 0,
                    updated_by    : 0
                }

        connection.query('INSERT INTO eod_uploads SET ? ON DUPLICATE KEY UPDATE poll_checksum = "' + md5 + '", attempts = attempts + 1, updated_by = 1', post, function(err, rows, fields) {
          if (err) {
            console.log(err);
            connection.end();
            callback(null,0);
          } else {
            console.log('Transaction Processed.');
            connection.end();
            callback(null, 1);
          }
        });
      }
    }
    ,function(err, results){
      if (err){ 
        console.log(err);
        removeTempFiles();
      } else if(results.insert) {
        console.log('Transaction Complete');
      }
    });
  }

  function eodFailed() {
    var loc_name = (row[0].name).replace(/\s+/g,'_').toLowerCase(),
        archive_path = '/home/www/poll/' + getNow(2) + '/' + loc_name + '/' + row[0].code + '/';

        async.series({
          'archive' : function(callback) {
            removeTempFiles();
            
            if(fs.existsSync(archive_path + 'POL' + store_code_parsed + '.zip')) {
              fs.unlinkSync(archive_path + 'POL' + store_code_parsed + '.zip');
            }

            if(fs.existsSync(archive_path + 'POL' + store_code_parsed + '.md5')) {
              fs.unlinkSync(archive_path + 'POL' + store_code_parsed + '.md5');
            }

            callback(null,1);
          },
          'insert' : function(callback) {
            var trans_id = checksum(row[0].code + Math.floor(new Date(getNow(2))/1000)),
                now = getNow(1);

            var post = {
                        id            : trans_id,
                        store_id      : row[0].id,
                        poll_path     : archive_path,
                        poll_checksum : '',
                        status        : 0,
                        attempts      : 1,
                        sales_date    : '',
                        total_sales   : 0,
                        created_at    : now,
                        updated_at    : now,
                        created_by    : 0,
                        updated_by    : 0
                    }

            connection.query('INSERT INTO eod_uploads SET ? ON DUPLICATE KEY UPDATE attempts = attempts + 1, updated_by = 1', post, function(err, rows, fields) {
              if (err) {
                console.log(err);
                connection.end();
                callback(null,0);
              } else {
                connection.end();
                callback(null,1);
              }
            });
          }
        }
        ,function(err, results){
          if (err)
            console.log(err);
        });
  }

  function checksum(str, algorithm, encoding) {
    return crypto
      .createHash(algorithm || 'md5')
      .update(str, 'utf8')
      .digest(encoding || 'hex');
  }

  function getNow(h) {
    d = new Date(),
    curr_month = d.getMonth() + 1,
    curr_date = d.getDate();
    curr_min = d.getMinutes();
    curr_sec = d.getSeconds();

    if(curr_month <= 9)
        curr_month = '0' + curr_month;

    if(curr_date <= 9)
      curr_date = '0' + curr_date;

    if (h == 1){
      curr_min = curr_min + '';
      if (curr_min.length == 1)
        curr_min = '0' + curr_min;

      curr_sec = curr_sec + '';
      if (curr_sec.length == 1)
        curr_sec = '0' + curr_sec;

      return d.getFullYear() + '-' + curr_month + '-' + curr_date + ' ' + d.getHours() + ':' + curr_min + ':' + curr_sec;
    } else if(h == 2) {
      return d.getFullYear() + curr_month + curr_date;
    }else {
      return d.getFullYear() + '-' + curr_month + '-' + curr_date;
    }
  }

  function removeTempFiles() {
    var dfiles = [tmp_path + 'poll04.dos', tmp_path + 'poll04a.dos', tmp_path + 'poll05.dos', tmp_path + 'POL' + store_code_parsed + '.zip', tmp_path + 'POL' + store_code_parsed + '.md5'];

    dfiles.forEach(function(filename) {
      if(fs.existsSync(filename)) {
        fs.unlink(filename);
      }
    });
  }

});

server.listen(5000, '172.31.1.3', function() {
  console.log('Server start listening on Localhost:5000');
});
