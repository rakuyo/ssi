<?php

include("jdatelnet.php");				// include the jdatelnet.php class
#var_dump($argv);
$ip_pattern = '/^\d{1,3}(\.\d{1,3}){3}$/';
$code_pattern = '/^\d{5}$/';

if(!isset($argv[1]) || !isset($argv[2])){
	echo "php jda4r4.php [jda_server_ip] [store_code] \r\n";
    exit(0);
}

if(!preg_match($ip_pattern,$argv[1]) || !preg_match($code_pattern,$argv[2])){
	echo "invalid script parameters!\r\nphp jda4r4.php [jda_server_ip] [store_code] \r\n";
	exit(0);
}

#else
#echo "ip and code are valid";

$jda = new jdatelnet($argv[1]);		// instantiate a new jdatelnet object and specify the IP address of the JDA server

$jda->login("TESTSYS","PASSWORD","MMRSTLIB");	// login by changing the username/password/library
$jda->write(ENTER,true);	
#print_r($jda->screen);					// view the JDA screen with this command
#echo "o0o\n----------------------\n";			// just make a separator for the next screen

function display($screen,$width=80){
	global $scrCounter;
	$scrCounter++;
	printf("%04d",$scrCounter);
	for($i = 4; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
	print_r(chunk_split($screen,$width));
	
	for($i = 0; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
}

function retreat($times = 2,$marker="MENTB4"){
	global $jda; 
	$i = 0;
	while($i++ < $times && !$jda->screenWait($marker,1)){
		$jda->write(null,F1,true);		
		display($jda->screen,80);
		echo "pressed F1 to return";
	}
}


function set_timer($label){
	global $timers,$last_time,$start_time;
	$now_time = microtime(true);
	$interval = $now_time - $last_time;
	$elapsed = $now_time - $start_time;
	$last_time = $now_time;
	$timer = array("label"=>$label,"now" =>$now_time,"interval"=>$interval, "elapsed"=>$elapsed);
	$timers[] = $timer;
	return $timer;
}

#var_dump($argv); 
$jda->write(ENTER,true);display($jda->screen);
$jda->write("28",true); display($jda->screen);
$jda->write("02",true); display($jda->screen);
$jda->write("17",true); display($jda->screen);
$jda->write("19",true); display($jda->screen);
$jda->write(TAB,true);
$jda->write($argv[2],true);display($jda->screen);
$jda->write("02",true);display($jda->screen);
$jda->write("Y",true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write(F7,true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write(F7,true);display($jda->screen);

?>