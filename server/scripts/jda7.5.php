<?php
/*
include("jda5250.php");				// include the jdatelnet.php class

$display_id = 4;
$jda = new jdatelnet("172.16.1.4",$display_id);		// instantiate a new jdatelnet object and specify the IP address of the JDA server
$jda->debug_level = 0; # Set to 0 for production, set to 1 or 2 for debugging
$jda->screenWait("Password");
$jda->login("TESTSYS2","PASSWORD","MMRMSLIB");	// login by changing the username/password/library
display($jda->screen,132);					     // view the JDA screen with this command

if($jda->screenCheck("Attempt to Recover Interactive Job")) {
	echo " Resetting...\n";
	$jda->write5250(array(array("90",22,07)),ENTER,true);
	$params=array();
	$params[] = array(6,53,"YES ");
	$params[] = array(7,53,"YES");
	$jda->write5250($params,ENTER,true);
	echo " Done!\n";
	$jda->close();
	unset($jda);
	sleep(10);
	# login again
	$jda->debug_level = 0; # Set to 0 for production, set to 1 or 2 for debugging
	$jda->screenWait("Password");
	display($jda->screen,132);					// view the JDA screen with this command
	$jda->login("TESTSYS2","PASSWORD","MMRMSLIB");	// login by changing the username/password/library
}
display($jda->screen,132);					     // view the JDA screen with this command

$timers = array();
$start_time = $last_time = microtime(true);

function set_timer($label){
	global $timers,$last_time,$start_time;
	$now_time = microtime(true);
	$interval = $now_time - $last_time;
	$elapsed = $now_time - $start_time;
	$last_time = $now_time;
	$timer = array("label"=>$label,"now" =>$now_time,"interval"=>$interval, "elapsed"=>$elapsed);
	$timers[] = $timer;
	return $timer;
}


function show_timers(){
	global $timers;
	printf("%20s%10s%10s%10s\n","Label", "Interval","Interval","Elapsed");
	foreach($timers as $timer){
		extract($timer);
		$mins = floor($interval / 60);
		$secs = $interval % 60;
		$interval_mins = "$mins:$secs";
		printf("%20s%10d%10s%10d\n",$label, $interval,$interval_mins,$elapsed);
	}
}
function display($screen,$width=132){
	global $scrCounter;
	$scrCounter++;
	printf("%04d",$scrCounter);
	for($i = 4; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
	print_r(chunk_split($screen,$width));
	
	for($i = 0; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
}
*/

include("jdatelnet.php");				// include the jdatelnet.php class

$ip_pattern = '/^\d{1,3}(\.\d{1,3}){3}$/';
$code_pattern = '/^\d{5}$/';

if(!isset($argv[1]) || !isset($argv[2])){
	echo "php jda4r4.php [jda_server_ip] [store_code] \r\n";
    exit(0);
}

if(!preg_match($ip_pattern,$argv[1]) || !preg_match($code_pattern,$argv[2])){
	echo "invalid script parameters!\r\nphp jda4r4.php [jda_server_ip] [store_code] \r\n";
	exit(0);
}

$jda = new jdatelnet($argv[1]);		// instantiate a new jdatelnet object and specify the IP address of the JDA server

$jda->login("TESTSYS2","PASSWORD","MMRSTLIB");	// login by changing the username/password/library
$jda->write(ENTER,true);	
#print_r($jda->screen);					// view the JDA screen with this command
#echo "o0o\n----------------------\n";			// just make a separator for the next screen


function display($screen,$width=80){
	global $scrCounter;
	$scrCounter++;
	printf("%04d",$scrCounter);
	for($i = 4; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
	print_r(chunk_split($screen,$width));
	
	for($i = 0; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
}

function retreat($times = 2,$marker="MENTB4"){
	global $jda; 
	$i = 0;
	while($i++ < $times && !$jda->screenWait($marker,1)){
		$jda->write(null,F1,true);		
		display($jda->screen,80);
		echo "pressed F1 to return";
	}
}


function set_timer($label){
	global $timers,$last_time,$start_time;
	$now_time = microtime(true);
	$interval = $now_time - $last_time;
	$elapsed = $now_time - $start_time;
	$last_time = $now_time;
	$timer = array("label"=>$label,"now" =>$now_time,"interval"=>$interval, "elapsed"=>$elapsed);
	$timers[] = $timer;
	return $timer;
}


$jda->write(ENTER,true);display($jda->screen);
$jda->write("28",true);display($jda->screen);
$jda->write("02",true);display($jda->screen);
$jda->write("22",true);display($jda->screen);
$jda->write("26",true);display($jda->screen);
$jda->write("21",true);display($jda->screen);
$jda->write("SALESADT",true);display($jda->screen);
$jda->write($argv[2],true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write(F7,true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write("TENDERS",true);display($jda->screen);
$jda->write(TAB,true);
$jda->write($argv[2],true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write(F7,true);display($jda->screen);
$jda->write(ENTER,true);display($jda->screen);
$jda->write(F1,true);display($jda->screen);
$jda->write(F7,true);display($jda->screen);

?>