var Connection = require('ssh2'),
    conn = new Connection(),
    args = process.argv.slice(2);

var command = 'cd /usr/local/cyware/retail/jobqueue/bin && ./pos_export_poll_manual.exe 1 1 /usr/local/cyware/retail/jobqueue/bin /home/www/poll/ ' + args[0]  + ' ' + args[1] +  
	      ' && ./poll_send.exe && /usr/local/imperium/retail/jobqueue/bin/client/./send_eod.sh';

function onReady() {
  conn.exec(command, onExec);
}

function onExec(err, stream) {  
  if (err) throw err;

  stream.on('exit', function(code, signal) {
    console.log('Stream :: exit :: code: ' + code + ', signal: ' + signal);
  });

  stream.on('close', function() {
    console.log('Stream :: close');
    conn.end();
  });

  stream.on('data', function(data) {
    console.log('STDOUT: ' + data);
  });

  stream.stderr.on('data', function(data) {
    console.log('STDERR: ' + data);
  });
}

conn.connect({  
  host: '172.31.1.60',
  port: 22,
  username: 'francis',
  password: 'francis.imperium'
});

conn.on('ready', onReady);
