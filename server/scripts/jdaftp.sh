#!/bin/bash

if [[ -z "$1" || -z "$2"  || -z "$3" || -z "$4" ]]; then
echo '[poll_path] [jda_ip] [jda_path] [poll_name]'
exit 0
fi

POLL_PATH=$1
JDA_IP=$2
JDA_PATH=$3
POLL_NAME=$4

echo "Positional Parameters"
echo '$0 = ' $0
echo '$1 = ' $1
echo '$2 = ' $2
echo '$3 = ' $3
echo '$4 = ' $4

cd $POLL_PATH
/usr/bin/ftp -i -n -v $JDA_IP <<EOF
user TESTSYS PASSWORD
cd $JDA_PATH
bin
hash
mget $POLL_NAME
bye
EOF