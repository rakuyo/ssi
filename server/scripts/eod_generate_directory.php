<?php

require 'vendor/autoload.php';

ActiveRecord\Config::initialize(function($cfg)
 {
     $cfg->set_model_directory('models');
     $cfg->set_connections(array(
         'development' => 'mysql://root:mysqladmin@localhost/ssi'));
 });
/*
$active_stores = Store::find_all_by_active(1);
$active_locations = Location::find_all_by_active(1);
$stores = array();
$stores_locations = array();
$locations = array();

foreach($active_stores as $active_store){
    $stores[] = $active_store->to_array(array('only' => array('id','location_id','code')));
    $stores_locations[] = array('id' => $active_store->id, 'locations' => $active_store->location->name, 'code' => $active_store->code);
}

foreach($active_locations as $active_location){
    $locations[] = $active_location->to_array(array('only' => array('id','name')));
}*/

#create current directory
function generate_directory($path){
    $active_stores = Store::find_all_by_active(1);
    if(is_dir("/home/www/poll")){
        echo "poll directory already exists <br>";
    }
    else{
        mkdir("/home/www/poll", 0755, true);
        echo "poll directory created <br>";
    }
    if(is_dir("/home/www/poll/$path")){
        echo "$path directory already exists <br>";
    }
    else{
        mkdir("/home/www/poll/$path", 0755, true);
        echo "$path directory created <br>";
    }
    foreach($active_stores as $store){
        $location = strtolower(str_replace(" ","_",$store->location->name));
        $store_code = $store->code;
        if(is_dir("/home/www/poll/$path/$location")){
            echo "$location already exists <br>";
        }
        else{
            mkdir("/home/www/poll/$path/$location", 0755, true);
            echo "$location created <br>";
        }
        if(is_dir("/home/www/poll/$path/$location/$store_code")){
            echo "$store_code already exists <br>";
        }
        else{
            mkdir("/home/www/poll/$path/$location/$store_code", 0755,true);
            echo "$store_code created <br>";
        }
    }
}

generate_directory("current");
generate_directory("last");
generate_directory(date('Ymd'));
#generate_directory("20140306");
?>
