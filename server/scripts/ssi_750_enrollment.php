#!/usr/bin/php
<?php

include("jda5250.php");				// include the jdatelnet.php class
include("../odbc.php");
$db1 = mysql_connect("172.16.1.171","rmsi_user","europe");
mysql_select_db("rmsi_edi");
# set debug level
# 0 - no display
# 1 - summary result display
# 2 - screen displays
$debug_level = 2;


if($argc > 2){
	$modulo = $argv[1];
	$instance = $argv[2];
}

$timers = array();
$start_time = $last_time = microtime(true);

function set_timer($label){
	global $timers,$last_time,$start_time;
	$now_time = microtime(true);
	$interval = $now_time - $last_time;
	$elapsed = $now_time - $start_time;
	$last_time = $now_time;
	$timer = array("label"=>$label,"now" =>$now_time,"interval"=>$interval, "elapsed"=>$elapsed);
	$timers[] = $timer;
	return $timer;
}


function show_timers(){
	global $timers;
	printf("%20s%10s%10s%10s\n","Label", "Interval","Interval","Elapsed");
	foreach($timers as $timer){
		extract($timer);
		$mins = floor($interval / 60);
		$secs = $interval % 60;
		$interval_mins = "$mins:$secs";
		printf("%20s%10d%10s%10d\n",$label, $interval,$interval_mins,$elapsed);
	}
}
function display($screen,$width=132){
	global $scrCounter;
	$scrCounter++;
	printf("%04d",$scrCounter);
	for($i = 4; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
	print_r(chunk_split($screen,$width));
	
	for($i = 0; $i<$width; $i++){
		echo "-";
	}
	echo "\n";
}

function find_sku_number($screen){
	#$pattern = "SKU Number:";
	#Number:       10311
	if(preg_match("%SKU Number:\s+([0-9]+)%",$screen,$screen_parts)){
		$sku_number = $screen_parts[1];
		echo "Found SKU Number $sku_number\n";
	}

	else {
		echo "Did not find SKU Number";
		echo "$screen";
	}
	$result = ($sku_number > 0) ? $sku_number : 0;
	return $result;
}


function get_skus_for_enrollment(){ 
	global $db1, $modulo, $instance,$mode,$location,$vendor;
	$todate = date("Y-m-d");
	
	if($modulo > 0 && $instance >=0){
		$sqlSelect = "SELECT * from sku_details where status=0 and id % $modulo = $instance ";
	}
	else{
		#$sqlSelect = "SELECT * from sku_details where  status = 0 and strokeno = 0 ; ";
		$sqlSelect = "SELECT * FROM sku_details WHERE company_sku = 8680129;  ";

	}
	#echo $sqlSelect;
	$result = mysql_query($sqlSelect);
	#echo $result;
	while($row = mysql_fetch_assoc($result)){
		$rows[] = $row;
	}
	return $rows;
}


function check_warnings($times = 3){
	global $jda;
	$i = 0;
	do{
		$jda->set_pos(6,8);
		$jda->write5250(null,ENTER,true);
		#echo "check_warning $i ";
	}while($i++ < $times && $jda->screenCheck("*Warning *"));
	# every successive ENTER clears a warning, but reveals the next
}
function retreat($times = 6,$marker="MENTB4"){
	global $jda; 
	$i = 0;
	while($i++ < $times && !$jda->screenWait($marker,3)){
		$jda->write5250(null,F1,true);		
		display($jda->screen,132);
		echo "pressed F1 to return";
	}
}
function create_new_sku($sku_detail){
	global $jda, $debug_level;

	extract($sku_detail);
	##Start here
	$jda->screenWait("MST001"); # Item Master Maintenance
	if($debug_level > 0) display($jda->screen,132);
	### STEP 1
	# for new SKU, press F7 to get a new SKU number
	
	if( false && $jda_sku > 0){	    
		$updating = true;
		# enter the old SKU number
		if($debug_level > 1) echo "Entering SKU # $jda_sku\n";
		#$jda->write($jda_sku.TAB,true);
		$jda->write5250(array(array(sprintf("%10d",$jda_sku),4,34)),TAB,true);
		$jda->write5250(array(array("01",22,44)),ENTER,true);		
		# Base Data Maintenance
		$jda->screenWait("MST018");
		if($debug_level > 0) display($jda->screen,132);
		# move to description field
		#$jda->write(TAB,true);
	}
	else{
	    $updating = false;		
		echo "Pressing F7 for a new SKU\n";
		$jda->write5250(null,F7,true); # F7 to create a new SKU				
		$jda->screenWait("New Item Options"); # New Item Options Press Menu option 3		
		$jda->write5250(array(array("3",17,24)),ENTER,true);		
		# Base Data Maintenance
		$jda->screenWait("MST018");
		$jda_sku = find_sku_number($jda->screen);
	}
	### STEP 2 Page 1 of Item Information
	
	$jda->screenWait("SKU Number");
	echo "SKU Number # $jda_sku\n";


	$jda->write5250(array(array($description,7,20)),ENTER,true);
	$jda->write5250(array(array("SYS",9,20)),ENTER,true);	
	$jda->write5250(array(array("05",13,20)),ENTER,true);	
	$jda->write5250(null,ENTER,true);
	#$jda->write5250(null,TAB,true);
	#$jda->write5250(null,TAB,true);
	#$jda->write5250(null,TAB,true);
	#$jda->write5250(null,TAB,true);
	#$jda->write5250(array(array($company_sku,15,62)),ENTER,true);	
	#$jda->write5250(null,ENTER,true);
	$jda->write5250(array(array($idept,17,11)),ENTER,true);
	$jda->write5250(null,ENTER,true);
	$jda->write5250(array(array($isdept,17,52)),ENTER,true);
	$jda->write5250(null,ENTER,true);
	$jda->write5250(array(array($iclas,18,11)),ENTER,true);
	$jda->write5250(null,ENTER,true);	
	$jda->write5250(array(array($isclas,18,52)),ENTER,true);
	$jda->write5250(null,ENTER,true);	
	
	if($debug_level > 0) display($jda->screen,132);
	$jda->screenWait("a moment",1); 
	#error checking after header input    

	
	
	if ($jda->screenCheck("This Vendor part number already exists for this Vendor")) {
		echo "Found pre-existing vendor part number\n";
		$sqlUpdate = "UPDATE sku_details set status = 3
			where vendor = '$vendor' and company_sku='$company_sku'";
		mysql_query($sqlUpdate);
		$jda->write5250(null,F1,true);
		$jda->write5250(null,F1,true);
		
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	
	elseif ($jda->screenCheck("A Sub-Class Number must be entered for the item")) {
		echo "Missed the sublcass number\n";
		$sqlUpdate = "UPDATE sku_details set status = 3
			where vendor = '$vendor' and company_sku='$company_sku'";
		mysql_query($sqlUpdate);
		$jda->write5250(null,F1,true);		
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	elseif ($jda->screenCheck("A sku number must be entered.")) {
		echo "Something Came UP!!! \n";
		#$sqlUpdate = "UPDATE sku_details set status = 3
		#	where vendor = '$vendor' and vendor_code = '$vendor_code' and company_sku='$company_sku'";
		#mysql_query($sqlUpdate);
		$jda->write5250(null,ENTER,true);
		$jda->write5250(null,ENTER,true);
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	elseif ($jda->screenCheck("This job has been placed on a batch Job Queue")) {
		echo "Something Came UP!!! \n";
		#$sqlUpdate = "UPDATE sku_details set status = 3
		#	where vendor = '$vendor' and vendor_code = '$vendor_code' and company_sku='$company_sku'";
		#mysql_query($sqlUpdate);
		$jda->write5250(null,ENTER,true);
		$jda->write5250(null,ENTER,true);
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	elseif ($jda->screenCheck("The UPC already exists for a different product.")) {
		echo "Something Came UP!!! \n";
		$sqlUpdate = "UPDATE sku_details set status = 4
			where vendor = '$vendor' and company_sku='$company_sku'";
		mysql_query($sqlUpdate);
		$jda->write5250(null,END,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,F7,true);
		
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	elseif ($jda->screenCheck("The size code is invalid")) {
		echo "Something Came UP!!! \n";
		#$sqlUpdate = "UPDATE sku_details set status = 3
		#	where vendor = '$vendor' and vendor_code = '$vendor_code' and company_sku='$company_sku'";
		#mysql_query($sqlUpdate);
		$jda->write5250(null,END,true);
		$jda->write5250(null,F7,true);
		if($debug_level > 1) display($jda->screen,132);
		retreat();
		return;
	}
	
	else{
	
		// update sku_details
		$sqlUpdate = "UPDATE sku_details set jda_sku = $jda_sku
			where vendor = '$vendor' and company_sku='$company_sku'";
		mysql_query($sqlUpdate);
	}
	
	
	
	    echo "Pressing F7 for a saving the first step\n";
		$jda->write5250(null,F7,true);	
   
	/*
	if($updating) {
		# updating will return back to main screen
		$jda->screenWait("MST001"); #Item Cost and Price Maintenance
		if($debug_level > 0) display($jda->screen,80);	
		$jda->write5250(TAB."02".ENTER,true);
		$jda->screenWait("MST011"); #Item Cost and Price Maintenance
		if($debug_level > 0) display($jda->screen,80);
		$jda->write5250(TAB.TAB.TAB.TAB.TAB,true);
	}
	else{
		if(!$jda->screenWait("MST011",6)){; #Item Cost and Price Maintenance
			echo "did not move forward, but got an SKU";
			// update sku_details
			$sqlUpdate = "UPDATE sku_details set status=3
				where vendor = '$vendor' and vendor_code = '$vendor_code'";
			mysql_query($sqlUpdate);
			retreat();
			return;
		}
		if($debug_level > 0) display($jda->screen,80);	
		$jda->write5250(TAB.TAB.TAB.TAB.TAB,true);
	}
	*/

	
	echo "*******Item Cost and Price Maintenance Screen ******* \n";
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	# Enter Price
	echo "*******Price Screen ******* \n";
	$jda_price = number_format($price,2,".","");	
	echo "Entering JDA Price # $jda_price\n";
	$jda->write5250(array(array($jda_price,11,51)),true);
	#$jda->write5250(null,ENTER,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
	$jda->write5250(null,TAB,true);
       $jda->write5250(null,TAB,true);
	echo "*******BUY U/M  and SELL Screen ******* \n";
	$jda->write5250(array(array("PC",18,45)),true); # BUY U/M
       $jda->write5250(null,TAB,true);
	#$jda->write5250(null,ENTER,true);
	$jda->write5250(array(array("PC",19,45)),true); # SELL U/M
       $jda->write5250(null,TAB,true);
       $jda->write5250(null,END,true);
	#$jda->write5250(null,ENTER,true);
       $jda->write5250(array(array("1",20,45)),true); # STANDARD PACK
	
	$jda->screenWait("a refresh",1); #Item Cost and Price Maintenance
	if($debug_level > 0) display($jda->screen,132);		// Item Entry Screen
	if($jda->screenCheck("Vendor Cost cannot be zero")){
		echo "some kind of error came up";
	}
	else
	{
	    echo "Pressing F7 for a saving the 2nd step\n";
		### F7 to Accept
		$jda->write5250(null,F7,true);	
		
		
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		echo "*******Colour  and Size Screen ******* \n";	
		$jda->write5250(array(array($colour,17,42)),ENTER,true);
		$jda->write5250(null,ENTER,true);
		$jda->write5250(array(array($size,18,42)),ENTER,true);		
		$jda->write5250(null,ENTER,true);		
		echo "### Pressing F7 for a saving the Colour and Sized step ###\n";
		### F7 to Accept
		$jda->write5250(null,F7,true);
		### F7 to Save Save the NULL UPC
		$jda->write5250(null,F7,true);
        
		#$jda->write5250(null,F7,true);
		#display($jda->screen,132);
		
		# Write company_sku as UPC
		echo "*******UPC Screen ******* \n";
		echo "Entering UPC # $company_sku\n";	
        #display($jda->screen,132);		
		$jda->write5250(array(array($company_sku,10,13)),ENTER,true);		
		echo "****Done Entering UPC # $company_sku **** \n";
		display($jda->screen,132);		
		$jda->write5250(array(array("PC",10,42)),ENTER,true);
		display($jda->screen,132);
		#$jda->write5250(null,ENTER,true);	
        display($jda->screen,132);		
		#accept UPC
		echo "Pressing F7 for a saving the UPC\n";
		$jda->write5250(null,F7,true);	
		
		
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		$jda->write5250(null,TAB,true);
		
		echo "*******Season Screen ******* \n";
		echo "Entering Season # $season\n";
		$jda->write5250(array(array($season,7,24)),ENTER,true);
		display($jda->screen,132);		
		#$jda->write5250(null,ENTER,true);		
		# accept Season
		echo "Pressing F7 for a saving the Season\n";
		display($jda->screen,132); 
		$jda->write5250(null,F7,true);			
		#$jda->write5250(null,F1,true);
		#$jda->write5250(null,ENTER,true);
		#$jda->write5250(null,ENTER,true);
		display($jda->screen,132);
		
		
	    
		
		$sqlUpdate = "UPDATE sku_details set status = 1
			where vendor = '$vendor' and vendor_code = '$vendor_code' and company_sku='$company_sku'";
		mysql_query($sqlUpdate);
		
			
	}
	
	retreat();

}

function validate_skus(){
	global $AS400;
		
	$todate = date("Y-m-d");
	$sqlSelect = "SELECT distinct inumbr from trapodet where tradate='$todate'";
	#echo $sqlSelect;
	$result = mysql_query($sqlSelect);
	$skulist = 100;
	while($row = mysql_fetch_assoc($result)){
		$skulist .= ", ".$row['inumbr'];
	}
	$sqlSelect = "SELECT INUMBR, IDSCCD FROM INVMST WHERE IDSCCD <> 'A' and INUMBR in ($skulist)";
	#echo $sqlSelect;
	$details = odbc_exec($AS400,$sqlSelect);
	while(	$row = @odbc_fetch_array($details)){
		// delete from the trapodet
		$sqlDelete = "delete from trapodet where tradate = '$todate' and inumbr = {$row['INUMBR']}";
		mysql_query($sqlDelete);
		echo "Clearing Invalid SKU No: {$row['INUMBR']}\n";
		
	}
}


/**
* To enroll JDA SKU's
* 1. Extract all SKU's for enrollment
* 2. Process each sku in sequence and capture new JDA SKU
* 3. Review SKU's
*/
#validate_skus();

set_time_limit(0);
$title = "JDA Create SKU's";
echo "<body style=\"font-size:x-small;\"><pre>\n";
/******** Login to JDA and head to Database Master Management Menu *********/
$nowdate = date("Y-m-d h:i:s");
echo " Processing RMSI SKU's in 7.5. Started $nowdate... \n";
$jda = new jdatelnet("172.16.1.1",$display_id);		// instantiate a new jdatelnet object and specify the IP address of the JDA server
$jda->debug_level = 0; # Set to 0 for production, set to 1 or 2 for debugging
$jda->screenWait("Password");
$jda->login("RMSTEST","PASSWORD","MMRMSLIB");	// login by changing the username/password/library
display($jda->screen,132);					     // view the JDA screen with this command
if($jda->screenCheck("Attempt to Recover Interactive Job")) {
	echo " Resetting...\n";
	$jda->write5250(array(array("90",22,07)),ENTER,true);
	$params=array();
	$params[] = array(6,53,"YES ");
	$params[] = array(7,53,"YES");
	$jda->write5250($params,ENTER,true);
	echo " Done!\n";
	$jda->close();
	unset($jda);
	sleep(10);
	# login again
	$jda = new jdatelnet("172.16.1.1",$display_id);		// instantiate a new jdatelnet object and specify the IP address of the JDA server
	$jda->debug_level = 0; # Set to 0 for production, set to 1 or 2 for debugging
	$jda->screenWait("Password");
	display($jda->screen,132);					// view the JDA screen with this command
	$jda->login($login_user,"PASSWORD","MMRMSLIB");	// login by changing the username/password/library
	
}
while($jda->screenWait("Press Enter to continue")) {
	$jda->write5250(null,ENTER,true);
}
# Wait for the main menu
if(!$jda->screenWait("Merchandising Systems")){
	if($jda->screenCheck("Press Enter to continue")) {
		echo "pressing Enter";
		$jda->write5250(null,ENTER,true);
	}
	if(!$jda->screenWait("Merchandising Systems")){
		if($jda->screenCheck("Press Enter to continue")) {
			echo "pressing Enter";
			$jda->write5250(null,ENTER,true);
		}
	}
}	
$jda->screenWait("Database/Master Files");
if($debug_level > 0) display($jda->screen,132);					// show the screen again.

$jda->write5250(array(array("5",22,44)),ENTER,true);
if($debug_level > 0) display($jda->screen,132);					// Database / File Maintenance
$jda->screenWait("MENMNT");

$jda->write5250(array(array("5",22,44)),ENTER,true);
display($jda->screen,132);
if($debug_level > 0) display($jda->screen,132);					// Product Maintenance
$jda->screenWait("MENTB2");

$jda->write5250(array(array("2",22,44)),ENTER,true);
if($debug_level > 0) display($jda->screen,132);					// Setup/Maintenance
$jda->screenWait("MENTB4");

/***************Process pending orders ****************/
$pending = get_skus_for_enrollment();
if($debug_level > 2) print_r($pending);
if(is_array($pending))
	foreach($pending as $key=>$sku_detail){
		if($debug_level > 2) print_r($sku_detail);
		$ctr = $key +1;
		set_timer("process sku");
		if(is_array($sku_detail)){
			$jda->screenWait("MENTB4");
			$jda->write5250(array(array("3",22,44)),ENTER,true);
			if($debug_level > 2) display($jda->screen,132);					// Item Master Maintenance
			$jda->screenWait("MST001");
			create_new_sku($sku_detail);
		}
	}
	
/********* Done! Logoff JDA *********/
# F1 to Return
$tries = 0;
while($tries++ < 5 && !$jda->screenCheck("F7=Signoff")){
	$jda->write5250(null,F1,true);
}	
if($debug_level > 2) echo "F1 to return\n";
if($debug_level > 2) display($jda->screen,132);					
# F7 to signoff
$jda->write5250(null,F7,true);		// Enter F7 to signoff
if($debug_level > 2) echo "F7 to signoff\n";
if($debug_level > 0) display($jda->screen,132);		
$jda->close();			

show_timers();
echo "</pre></body></html>\n";
?>
