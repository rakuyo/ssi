<!DOCTYPE html>
<html lang="en">
<head>
       
        <!--<script src="bootstrap/js/jquery.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>   
        -->
        <link href="css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/ssi.css"> 
      <!--angular not working    // <script type="text/javascript" href="js/angular/angular.min.js"></script> -->
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>     
</head>
<body>
<div class="container ">
    <div class="row clearfix row-fluid">
        <div id="main_navbar" >
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <!-- Responsive navigation bar on mobile devices -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="glyphicon glyphicon-align-justify"></span><!-- Responsive button icon -->
                    </button>
                </div>
                <nav class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <!-- Highlighted menu for current page -->
                            <a id="dashboard" href="layout.php"><h2 class="glyphicon glyphicon-dashboard text-color"></h2><br><h4> Dashboard </h4> </a>
                        </li>
                        <li class="current">
                            <a id="brands" href="stores.php"><h2 class = "glyphicon glyphicon-th text-color"></h2><br><h4>Brands</h4></a>
                        </li>
                        <li>
                            <a  id="in-progress"  href="inProgress.php"><h2 class="glyphicon glyphicon-tasks text-color"></h2><br> <h4>In Porgress</h4></a>
                        </li> 
                        </ul>
               
                 <div class="btn-group" id="Admin-btn"> 
                    <button type="button" class="btn btn-default dropdown-toggle glyphicon glyphicon-user" data-toggle="dropdown" aria-expanded="false">
                        Admin <span class="caret"></span>
                    </button>
                <ul class="dropdown-menu" role="menu">
                <li><a href="#">Log-out</a></li>
                </ul>
                </div>
                </nav>
            </nav>
        </div>
        <div class="col-md-12 column">
        <div class="panel panel-primary brand-panel">
                <div class="panel-heading">
                    <h3 class="panel-title ">
                        <span class="glyphicon glyphicon-th text-color"></span> Brands
                    </h3>
                </div>
                <div class="panel-body row row-fluid">
                    <table class="table  table-hover table-striped table-brand">
                            <tr>
                                <thead>
                                <th>Brand Name</th> <th>Store Name</th> <th>Sales Date</th><th>End of Sales Status</th> <th>Attempt</th>
                                </thead>
                            <tbody>

                          <!--   </tr>
                            <tr class="danger" >
                                <td>Lacoste</td><td>Makati</td><td>1-26-2015</td><td>Failed</td><td>Initial</td>
                            </tr> 
                            <tr class="danger">  
                                <td>Calvin Klein</td><td>Makati</td><td>1-26-2015</td><td>Failed</td><td>Initial</td>
                             </tr>   
                            <tr class="danger">    
                                <td>Fendi</td><td>Alabang</td><td>1-26-2015</td><td>Failed</td><td>Second</td>
                            </tr> 
                            <tr>      
                                <td>Lacoste</td><td>Shangri-la</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            </tr>   
                                <td>Natori</td><td>Alabang</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            </tr>
                            <tr class="info">   
                                <td>Natori</td><td>Alabang</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            </tr>
                            <tr class="info">   
                                <td>Natori</td><td>Alabang</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            <tr>
                            <tr class="success">   
                                <td>Natori</td><td>Alabang</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            </tr>
                             <tr class="success" >  
                                <td>Natori</td><td>Alabang</td><td>1-26-2015</td><td>None</td><td>Initial</td>
                            </tr> -->
                            </tbody>
                            </table>
                </div>
                <div class="panel-footer">              
                <form>
                   <button type="button" id="pdf" class="btn btn-default glyphicon glyphicon-print"> PDF</button>
                   <button type="button" id="csv" class="btn btn-default glyphicon glyphicon-file"> CSV</button>
                </form>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
