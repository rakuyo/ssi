<?php

require 'vendor/autoload.php';

function JSONResponse($status, $response = array())	{
	$app = \Slim\Slim::getInstance();
    $app->status($status);
    $app->contentType('application/json');
    echo json_encode($response);
}

 ActiveRecord\Config::initialize(function($cfg)
 {     	$cfg->set_model_directory('models');
    	$cfg->set_connections(array(
        	'development' => 'mysql://root:mysqladmin@172.31.1.3/ssi'));
 });



$app->run();


?>