<?php 

#Brands CRUD

require 'vendor/autoload.php';

function JSONResponse($status, $response = array())	{
	$app = \Slim\Slim::getInstance();
    $app->status($status);
    $app->contentType('application/json');
    echo json_encode($response);
}

 ActiveRecord\Config::initialize(function($cfg)
 {     	$cfg->set_model_directory('models');
    	$cfg->set_connections(array(
        	'development' => 'mysql://root:mysqladmin@172.31.1.3/ssi'));
 });
 	$app = new \Slim\Slim();
	
#Create Brands 

$app->post('/create/:name/:description/:active',function ($name,$description,$active)use ($app) {
 			  $brands = brands::create(array(
 			  	'name' => $name,
 			  	'description'=> $description,
 			  	'active' => $active
 			  	));	
});



#Read Brands	

$app->get('/status', function ()use ($app) {
	$brands = brands::all();
	$json = array();
	foreach ($brands as $data) {
		$json[] = $data->to_array();
	}

	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($brands){
	 	echo json_encode($response);
	 }
});
	/* Response Codes */    
 //    try {
 //    	$brands = brands::all();	
	// 	foreach ($brands  as $data)	{
	// 	    $response['data']= $data->to_json();
	// 	}
	// 	$response['data'] = $brands;
	// }catch (Exception $e) {
 //    	$status = 400; // Error
 //    	$response['status'] = 'error';
 //    	$response['message'] = 'Error on response: ' . $e->getMessage();
 //    }

    // print_r($response['data']);
	   			

#Update Brands
	$app->put('/update/id/:id', function ($id)use ($app) {
 			   $brands = brands::find($id);	
			   $brands->name = 'Updated';
	   		   $brands->save ();
	   		   echo $id." "."update"; 
	   		   echo "<br>";
	   		   echo $brands->id." ".$brands->name."  ".$brands->description." ".$brands->active;	   		
});

#Delete Brands

$app->delete('/delete/id/:id', function ($id)use ($app) {
 			   $brands = brands::find($id);	
	   		   $brands->delete ();
	   		   echo $id." "."Deleted ";

});		
#Brands Cruds end


$app->run();

?>
