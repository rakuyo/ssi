<!DOCTYPE html>
<html lang="en">
<head>
 <link href="css/bootstrap.min.css">
        <!--<script src="bootstrap/js/jquery.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>   
        -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/ssi.css">      
</head>
<body>
<div class="container ">
    <div class="row clearfix row-fluid">
        <div id="main_navbar">
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <!-- Responsive navigation bar on mobile devices -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="glyphicon glyphicon-align-justify"></span><!-- Responsive button icon -->
                    </button>
                </div>
                <nav class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <!-- Highlighted menu for current page -->
                            <a id="dashboard" href="layout.php"><h2 class="glyphicon glyphicon-dashboard text-color"></h2><br><h4> Dashboard </h4> </a>
                        </li>
                        <li>
                            <a id="brands" href="stores.php"><h2 class = "glyphicon glyphicon-th text-color"></h2><br><h4>Brands</h4></a>
                        </li>
                        <li class="current">
                            <a  id="in-progress"  href="inProgress.php"><h2 class="glyphicon glyphicon-tasks text-color"></h2><br> <h4>In Porgress</h4></a>
                        </li> 
                        </ul>
               
                 <div class="btn-group" id="Admin-btn"> 
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Admin <span class="caret"></span>
                    </button>
                <ul class="dropdown-menu" role="menu">
                <li><a href="#">Log-out</a></li>
                </ul>
                </div>
               
                </nav>
            </nav>
        </div>
        <div class="col-md-12 column">
   
                <div class="panel panel-primary brand-panel">
                <div class="panel-heading">
                    <h3 class="panel-title ">
                        <span class="glyphicon glyphicon-th-list"></span> In Progress
                    </h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                            <tr>
                                <thead>
                                <th>Brand Name</th> <th>Store Name</th> <th>Percentage</th>
                                </thead>
                            <tbody>
                            </tr>
                            <tr>
                                <td>Burberry</td><td>Gateway</td>
                                <td>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                            </tr> 
                            <tr>  
                                <td>burberry</td><td>Gateway</td>
                                <td>
                                    <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                             </tr>   
                            <tr>    
                                <td>Yves Saint Laurent</td><td>Alabang</td>
                                <td>
                                    <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                            </tr> 
                            <tr>      
                                <td>Gicci</td><td>Makati</td>
                                <td>
                                    <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                            </tr>   
                                <tr>      
                                <td>Gicci</td><td>Makati</td><td>
                                     <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                            </tr>   
                            <tr>      
                                <td>Kenzo</td><td>Makati</td><td>
                                    <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> <p>30%</p>
                                    </div>
                                </div>
                                </td>
                            </tr>   

                            </tbody>
                            </table>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
