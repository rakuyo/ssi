<?php 



require 'vendor/autoload.php';

function JSONResponse($status, $response = array())	{
	$app = \Slim\Slim::getInstance();
    $app->status($status);
    $app->contentType('application/json');
    echo json_encode($response);
}

 ActiveRecord\Config::initialize(function($cfg)
 {     	$cfg->set_model_directory('models');
    	$cfg->set_connections(array(
        	'development' => 'mysql://root:mysqladmin@localhost/ssi'));
 });
 	$app = new \Slim\Slim();
	
#Create Brands 

$app->post('/AddBrand',function ()use ($app) {
 	   
 	 if ($brand_url = $app->request->getBody()){		
		 $brand_add  = $brand_url;
		 parse_str($brand_add);
		 $brands = brands::create(array(
		'name'=> $BrandName,
		'description'=> $BrandDescription,
		'active'=> $Active,
		));
	}

});

#Read Brands	

$app->get('/brands', function ()use ($app){
	$brands = brands::all();
	$json = array();
	
	foreach ($brands as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($brands){
	 	echo json_encode($response);
	 	
	 }

});

#Update Brands
	$app->put('/UpdateBrand', function ()use ($app) {
 			 if($Edit_Brand_url = $app->request->getBody()){
 			  parse_str($Edit_Brand_url, $data);
			  $brands_update = brands::all($data['id']);	
			 
			  $brands_update->name = $data['UpdatedBrandName'];
			  $brands_update->description = $data['UpdatedDescription'];
			  $brands_update->active = $data['Active'];
			  $brands_update->save ();
	   		   	

			  die();
			 $json = array();
			 foreach ($brands_update as $modalData) {
			  			$json[] = $modalData->to_array();
				}
			 $app->response()->header("Content-Type", "application/json");
			 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
			if($brands_update){
			 echo json_encode($response);
			 }   		
		}
});

#  Brands UpdateStatusData

$app->put('/ActivateBrands/:id', function ($id)use ($app) {
 			   $brands = brands::find($id);	
 			   $brands->active = 1;
 			   $brands->save();
});
$app->put('/DeactivateBrands/:id', function ($id)use ($app) {
 			   $brands = brands::find($id);	
	   		   $brands->active = 0; 
	   		   $brands->save();  		   
});			
#Brands Cruds End


#poll50s READ


$app->get('/poll05s', function ()use ($app) {
	$poll05s = poll05s::all();
	$json = array();
	foreach ($poll05s as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($poll05s){
	 	echo json_encode($response);
	 }
});


#Eod_uploads READ

$app->get('/eod_uploads', function ()use ($app) {
	$eod_uploads = eod_uploads::all();
	$json = array();
	foreach ($eod_uploads as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($eod_uploads){
	 	echo json_encode($response);
	 }
});


#location CRUD


$app->get('/locations/', function ($id=null)use ($app) {
	$location = locations::all();
	$json = array();
	foreach ($location as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($location){
	 	echo json_encode($response);
	 }
});

// Location Create


$app->post('/AddLocation', function () use ($app) {
	
	if ($location_url= $app->request->getBody()){
	   	$location_add = $location_url;
		parse_str($location_add,$data);
		$location = locations::create(array(
		'name'=>$data['LocationName'],
		'description'=>$data['LocationDescription'],
		'active'=>'1',
		));
	}
});

// Update Location

$app->put('/UpdateLocation', function ()use ($app) {
 			 if($Edit_Location = $app->request->getBody()){
 			  parse_str($Edit_Location, $data);
			  $Location_update = locations::all($data['id']);	
			  var_dump($Location_update);
			  
			  $Location_update->name = $data['UpdatedLocation'];
			  $Location_update->description = $data['Description'];
			  $Location_update->save ();
	   		   	

			  die();
			 $json = array();
			 foreach ($brands_update as $modalData) {
			  			$json[] = $modalData->to_array();
				}
			 $app->response()->header("Content-Type", "application/json");
			 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
			if($brands_update){
			 echo json_encode($response);
			 }   		
		}
});




$app->put('/ActivateLocation/:id', function ($id)use ($app) {
 			   $brands = locations::find($id);	
 			   $brands->active = 1;
 			   $brands->save();
});
$app->put('/DeactivateLocation/:id', function ($id)use ($app) {
 			   $brands = locations::find($id);	
	   		   $brands->active = 0; 
	   		   $brands->save();  		   
});			

// Create JDA

$app->post('/AddJda', function () use ($app) {
	
	if ($jda_url= $app->request->getBody()){
	    
	 	$jda_add = $jda_url;
		parse_str($jda_add, $data);
		echo var_dump($data);
	 	$jda = jda::create(array(
		'name'=>$data['JdaName'],
		'ip'=>$data['IP'],
		'version'=>$data['JdaVersion'],
		'active'=>'1',
		));
	}
});

// Update JDA
$app->put('/UpdateJda', function ()use ($app) {
 			 if($Edit_Jda = $app->request->getBody()){
 			  parse_str($Edit_Jda, $data);
			  $Jda_update = jda::all($data['id']);	
			 
			  
			  $Jda_update->name = $data['UpdatedJdaName'];
			  $Jda_update->ip = $data['UpdatedIP'];
			  $Jda_update->version = $data['Active'];
			  $Jda_update->save ();
	   		   	

			  die();
			 $json = array();
			 foreach ($brands_update as $modalData) {
			  			$json[] = $modalData->to_array();
				}
			 $app->response()->header("Content-Type", "application/json");
			 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
			if($brands_update){
			 echo json_encode($response);
			 }   		
		}
});




// Delete JDA 

$app->put('/ActivateJda/:id', function ($id)use ($app) {
 			   $brands = jda::find($id);	
 			   $brands->active = 1;
 			   $brands->save();
});
$app->put('/DeactivateJda/:id', function ($id)use ($app) {
 			   $brands = jda::find($id);	
	   		   $brands->active = 0; 
	   		   $brands->save();  		   
});			

$app->get('/jda', function ()use ($app) {
	$jda = jda::all();
	$json = array();
	foreach ($jda as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($jda){
	 	echo json_encode($response);
	 }
});


# Create Store
$app->post('/AddStore',function ()use ($app) {
	
		
	if ($store_url = $app->request->getBody()){
		
		$store_add = $store_url;
		parse_str($store_add,$data);
		var_dump($data);
		$stores = stores::create(array(
		'location_id'=>$data['StoreId'],
		'brand_id'=>$data['BrandId'],
		'jda_id'=>$data['JdaID'],
		'jda_path'=>$data['JdaPath'],
		'code'=>$data['Code'],
		'ip'=>$data['IP'],
		'opening_time'=>$data['Opening'],
		'closing_time'=>$data['Closing'],
		'assigned_to'=>$data['Assigned'],
		'active'=>1
		));

	} 
});

$app->put('/ActivateStore/:id', function ($id)use ($app) {
 			   $brands = stores::find($id);	
 			   $brands->active = 1;
 			   $brands->save();
});
$app->put('/DeactivateStore/:id', function ($id)use ($app) {
 			   $brands = stores::find($id);	
	   		   $brands->active = 0; 
	   		   $brands->save();  		   
});			


#READ Stores
$app->get('/stores', function ()use ($app) {
	$stores = stores::find_by_sql("SELECT l.name as locationname, l.id AS locationid, b.name as brandname, b.id AS brand, j.name,j.id, j.ip AS jdas , s.jda_path, s.code, s.ip, s.opening_time, s.closing_time, s.assigned_to, s.active
	FROM stores s
	INNER JOIN locations l ON s.location_id = l.id
	INNER JOIN brands b ON s.brand_id = b.id
	INNER JOIN jdas j ON  s.jda_id = j.id ORDER BY s.id DESC");
	
	$json = array();
	foreach ($stores as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($stores){
	 	echo json_encode($response);
	 }
});



$app->get('/Done/:datetoday',function($datetoday)use ($app) {

$date = eod_uploads::find_by_sql("SELECT l.name AS location, b.name AS brand, s.code AS store, eod.total_sales, eod.created_at, eod.updated_at, eod.created_by, eod.updated_by
FROM eod_uploads eod
INNER JOIN stores s ON eod.store_id = s.id
INNER JOIN locations l ON s.location_id = l.id
INNER JOIN brands b ON s.brand_id = b.id
WHERE eod.attempts = 1
AND eod.status =1
AND sales_date = '$datetoday'");

$json = array();
foreach ($date as $data) {
		$json[] = $data->to_array();
		// array_push($json,$data->to_array());
	}
$app->response()->header("Content-Type", "application/json");
$response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
if ($date){
    echo json_encode($response);

}
});

$app->get('/Failed/:datetoday',function($datetoday)use ($app) {

$date = eod_uploads::find_by_sql("SELECT l.name AS location, b.name AS brand, s.assigned_to, s.ip, s.code, s.opening_time, s.closing_time, eod.attempts, eod.total_sales, eod.created_at, eod.updated_at, eod.created_by, eod.updated_by
FROM eod_uploads eod
INNER JOIN stores s ON eod.store_id = s.id
INNER JOIN locations l ON s.location_id = l.id
INNER JOIN brands b ON s.brand_id = b.id
WHERE eod.attempts > 0
AND eod.status = 0
AND sales_date = '$datetoday'" );
$json = array();

foreach ($date as $data) {
		$json[] = $data->to_array();
		// array_push($json,$data->to_array());
	}
$app->response()->header("Content-Type", "application/json");
$response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
if ($date){
    echo json_encode($response);

}

});

$app->get('/NoSales/:datetoday',function($datetoday)use ($app) {
// $date = eod_uploads::find('all', array('conditions' => ));
$date = eod_uploads::find_by_sql("SELECT l.name AS location, b.name AS brand, s.assigned_to, s.ip, s.code, s.opening_time, s.closing_time, eod.attempts, eod.total_sales, eod.created_at, eod.updated_at, eod.created_by, eod.updated_by
FROM eod_uploads eod
INNER JOIN stores s ON eod.store_id = s.id
INNER JOIN locations l ON s.location_id = l.id
INNER JOIN brands b ON s.brand_id = b.id
WHERE eod.attempts > 0
AND eod.status = 1
AND total_sales = 0
AND sales_date = '$datetoday'");
$json = array();

foreach ($date as $data) {
		$json[] = $data->to_array();
		// array_push($json,$data->to_array());
	}
$app->response()->header("Content-Type", "application/json");
$response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
if ($date){
    echo json_encode($response);

}

});

$app->get('/NoEndofSales/:datetoday',function($datetoday)use ($app) {
$date = eod_uploads::find_by_sql("SELECT s.code, s.assigned_to, s.ip, s.opening_time, s.closing_time, l.name AS location, b.name AS brand FROM stores s
INNER JOIN locations l ON s.location_id = l.id
INNER JOIN brands b ON s.brand_id = b.id
WHERE s.id NOT IN (SELECT store_id FROM `eod_uploads` WHERE  sales_date = '$datetoday')");
#$date = eod_uploads::find_by_sql("SELECT code FROM `stores` WHERE id NOT IN (SELECT store_id FROM `eod_uploads` WHERE  sales_date = '$datetoday')");
$json = array();
foreach ($date as $data) {
		$json[] = $data->to_array();
		// array_push($json,$data->to_array());
	}
$app->response()->header("Content-Type", "application/json");
$response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
if ($date){
    echo json_encode($response);

}
});

$app->get('/Graph/:datetoday',function($datetoday)use ($app) {

$done = eod_uploads::count(array('conditions' => array('status=? AND attempts = ? AND sales_date = ?', 1,1,$datetoday)));
$failed = eod_uploads::count(array('conditions' => array('status=? AND attempts > ? AND sales_date = ?', 0,0,$datetoday)));
$nosales = eod_uploads::count(array('conditions' => array('status=? AND attempts > ? AND total_sales = ?  AND sales_date = ? ', 1,0,0,$datetoday)));
$noendofsales = eod_uploads::find_by_sql("SELECT code FROM `stores` WHERE id NOT IN (SELECT store_id FROM `eod_uploads` WHERE  sales_date = '$datetoday')");
$noendofsales = count($noendofsales);


$done = (int)($done);
$failed = (int)($failed);
$nosales = (int)($nosales);
$noendofsales = (int)($noendofsales);

$GraphData = array($nosales,$noendofsales,$failed,$done);

$app->response()->header("Content-Type", "application/json");
 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $GraphData);
if ($done){
    echo json_encode($response);

}
});

// select data

$app->get('/store_names', function ()use ($app) {
	$store_names = locations::find_all_by_active(1);
	$json = array();
	foreach ($store_names as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($store_names){
	 	echo json_encode($response);
	 }
});


$app->get('/brand_names', function ()use ($app) {
	$brand_names = brands::find_all_by_active(1);
	$json = array();
	foreach ($brand_names as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($brand_names){
	 	echo json_encode($response);
	 }
});

$app->get('/jda_names', function ()use ($app) {
	$jda_names = jda::find_all_by_active(1);
	$json = array();
	foreach ($jda_names as $data) {
		$json[] = $data->to_array();
	}
	 $app->response()->header("Content-Type", "application/json");
	 $response = array('status' => 'success', 'message' => 'Response Success', 'data' => $json);
	 if ($jda_names){
	 	echo json_encode($response);
	 }
});


	
$app->run();

?>
