'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:JdaJdaCtrl
 * @description
 * # JdaJdaCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('JdaJdaCtrl', function ($scope,CRUD,$route) {
  
	$scope.items = {};

    $scope.jda = function(){
		CRUD.get('/jda').then(function(response){
		 $scope.jda = response.data;
			console.log ($scope.jda);
		});	
	}
   
   $scope.AddJda = function(){
  	
  	var obj = { 
		'JdaName':$scope.JdaName,
	   	'IP':$scope.JdaIP,
	    'JdaVersion':$scope.JdaVersion,
	    
    };
	var params = $.param(obj);
	console.log(params);
	CRUD.post('/AddJda',params).then(function(response){      	

	    });
  	}
  	

  		// Init Edit Brands
   $scope.getJDA = function(data){
		$scope.items[data.id] = {
			id: data.id,
			name:data.name,
			ip: data.ip,
			version: data.version,
		} 	
	var obj = $scope.items[data.id];
	
	}
   $scope.getIndex = function(id)	{
		$scope.index = id;
		console.log($scope.index);
   var obj = $scope.items[$scope.index];
   var params = $.param(obj);
  		$scope.ID = obj['id'];
  		$scope.JdaName = obj['name']
  		$scope.JdaIP = obj['ip'];
	   	$scope.JdaVersion = obj['version'];
	   
	}	

	$scope.UpdateJda = function(){

	 var update = {
	 	'id':$scope.ID,
   		'UpdatedJdaName':$scope.JdaName ,	
		'UpdatedIP':$scope.JdaIP,
   		'Active':$scope.JdaVersion
   		}	
	var params = $.param(update);
	CRUD.put('/UpdateJda',params).then(function(response){      	
		$scope.EditBrand = response.data;
		alert("Update Success");
		
	    });
		

	}


$scope.Activate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/ActivateJda/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}
	$scope.Deactivate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/DeactivateJda/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}

 });
