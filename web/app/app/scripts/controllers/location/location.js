'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:LocationLocationCtrl
 * @description
 * # LocationLocationCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('LocationLocationCtrl', function ($scope,CRUD,$route) {
   
    $scope.items = {};

   $scope.init = function (){
  		$scope.LocationsTable();
  		
  	};
   $scope.LocationsTable = function(){
		CRUD.get('/locations').then(function(response)	{
		 $scope.LocationsTable = response.data;
			// console.log ($scope.LocationsTable);
			
		});	
	}
  
  $scope.AddLocation = function(){
    
    var obj = {
            'LocationName':$scope.LocationName,
            'LocationDescription':$scope.LocationDescription,  
        };
  			    var params = $.param(obj);
            CRUD.post('/AddLocation',params).then(function(response){      	
	    	$scope.LocationsTable
	    });
	};

// Edit Location


 $scope.getLocation = function(data){
    $scope.items[data.id] = {
      id: data.id,
      name:data.name,
      description: data.description,
      
    }   
  var obj = $scope.items[data.id];
  
  }
   $scope.getIndex = function(id) {
    $scope.index = id;
    console.log($scope.index);
   var obj = $scope.items[$scope.index];
   var params = $.param(obj);
      $scope.ID = obj['id'];
      $scope.LocationName = obj['name']
      $scope.LocationDescription = obj['description'];
           
  } 

  $scope.UpdateLocation = function(){

   var update = {
      'id':$scope.ID,
      'UpdatedLocation':$scope.LocationName , 
      'Description':$scope.LocationDescription,
      
      } 
  var params = $.param(update);
  CRUD.put('/UpdateLocation',params).then(function(response){        
    $scope.EditBrand = response.data;
    alert("Update Success");
    
      });
    

  }





  $scope.Activate = function(id){
    $scope.index = id;
    console.log ($scope.index); 
    CRUD.put('/ActivateLocation/'+$scope.index).then(function(response){       
    alert("Update Success");
    $route.reload();
    });
  }
  $scope.Deactivate = function(id){
    $scope.index = id;
    console.log ($scope.index); 
    CRUD.put('/DeactivateLocation/'+$scope.index).then(function(response){       
    alert("Update Success");
    $route.reload();
    });
  }

});
