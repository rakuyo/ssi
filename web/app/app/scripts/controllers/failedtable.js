'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:FailedtableCtrl
 * @description
 * # FailedtableCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('FailedtableCtrl', function ($scope,CRUD,$rootScope) {
   $scope.TableFailed = function() {
	 CRUD.get('/Failed/'+$rootScope.date).then(function(response)  {
        $scope.TableFailed = response.data;
     	 console.log($scope.TableFailed);	 
     });

	}
 });
