'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:EodUploadsEodUploadsCtrl
 * @description
 * # EodUploadsEodUploadsCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('EodUploadsEodUploadsCtrl', function ($scope,CRUD) {
    $scope.eod_uploads = function(){
		CRUD.get('/eod_uploads').then(function(response)	{
		 $scope.eod_uploads = response.data;
			console.log ($scope.eod_uploads);
		});	
	}
  });
