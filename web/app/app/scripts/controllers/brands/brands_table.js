'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:BrandsBrandsTableCtrl
 * @description
 * # BrandsBrandsTableCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('BrandsBrandsTableCtrl', function ($scope,CRUD,$route) {

  
  	$scope.items = {};

   $scope.brands = function(){
		CRUD.get('/brands').then(function(response)	{
		 $scope.brands = response.data;
			
		});	
	}
  
  	// Init Edit Brands
   $scope.getBrands = function(data){
		$scope.items[data.id] = {
			id: data.id,
			name: data.name,
			description: data.description,
			active: data.active
		} 	
	var obj = $scope.items[data.id];
	
	}
   $scope.getIndex = function(id)	{
		$scope.index = id;
		
   var obj = $scope.items[$scope.index];
   var params = $.param(obj);
  		$scope.ID = obj['id'];
  		$scope.EditBrandName = obj['name'];
	   	$scope.EditBrandDescription = obj['description'];
	   	$scope.EditActive = obj['active'];
   

   CRUD.put('/UpdateBrand',params).then(function(response){      	
		$scope.EditBrand = response.data;
		   
	    });
	
	}	

	$scope.UpdateBrand = function(){

	 var update = {
	 	'id':$scope.ID,
   		'UpdatedBrandName':$scope.EditBrandName ,	
		'UpdatedDescription':$scope.EditBrandDescription,
   		'Active':$scope.EditActive
   		}	
	var params = $.param(update);
	CRUD.put('/UpdateBrand',params).then(function(response){      	
		$scope.EditBrand = response.data;
		alert("Update Success");
		$route.reload();
		
	    });
		

	}


$scope.Activate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/ActivateBrands/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}
$scope.Deactivate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/DeactivateBrands/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}



//Add Brands
 	$scope.AddBrand = function(){ 
 		
   
	var obj = { 
		'BrandName': $scope.BrandName,
	   	'BrandDescription':$scope.BrandDescription,
	    'Active':$scope.Active,
	 
  	};
	var params = $.param(obj);
	CRUD.post('/AddBrand',params).then(function(response){      	
		alert("New Data Added");
		$route.reload();

	    });
	}	


});
