'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:TestIndexCtrl
 * @description
 * # TestIndexCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('TestIndexCtrl', function ($scope, CRUD) {
	$scope.test = function(){
		CRUD.get('/brands').then(function(response)	{
		 $scope.test = response.data;
			console.log ($scope.test);
		});	
	}
});
