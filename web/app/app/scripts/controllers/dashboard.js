'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('DashboardCtrl', function ($scope,$rootScope,CRUD) {

  // Put all logic here necessary in startup
   $scope.init = function()	{
	   	$scope.getYesterday();  	
	   	// Do date pick
	   	$scope.datepicker();
   		//Dispaly Graph
   		$scope.Graphdatepicker();
   		$scope.GraphData();
    
   };


  // Init Variables
  $scope.data = [];
  $scope.counter = 0;
  // Initialize JQuery Datepicker
  $('#StatusDate').datepicker({
  	autoclose:true
  })
  $('#GraphDate').datepicker({
  	autoclose:true
  })

	$scope.datepicker= function() {
		$rootScope.date = $scope.date
		  
		 CRUD.get('/Done/'+$scope.date).then(function(response)  {
	        $scope.Done = response.data;
	     });

		 CRUD.get('/Failed/'+$scope.date).then(function(response)  {
	        $scope.Failed = response.data; 	 
	     });

		 CRUD.get('/NoSales/'+$scope.date).then(function(response)  {
	        var length = 0
	        $scope.NoSales = response.data;	
	     });	
		CRUD.get('/NoEndofSales/'+$scope.date).then(function(response)  {
	        $scope.NoEndofSales = response.data;
	       
	     });	
	
 }
 
   	//Generate Date Yesterday
   $scope.getYesterday = function()	{
   	var date = new Date();
   	date.setDate(date.getDate() -1);
 	// Change date value into string
   	var date_string = date.toLocaleDateString();
   	var date_array = date_string.split('/');

   	$scope.date = date_array[2] + '-' + ('0' + date_array[0]).slice(-2) + '-' + ('0' + date_array[1]).slice(-2);
  };

  $scope.GraphData = function()	{
   	var GraphDate = new Date();
   	GraphDate.setDate(GraphDate.getDate() -1);
 	// Change date value into string
   	var date_string = GraphDate.toLocaleDateString();
   	var date_array = date_string.split('/');

   	$scope.GraphDate = date_array[2] + '-' + ('0' + date_array[0]).slice(-2) + '-' + ('0' + date_array[1]).slice(-2);
  };

  $scope.Graphdatepicker = function() {

  	CRUD.get('/Graph/'+$scope.GraphDate).then(function(response)  {
	        $scope.Graph = response.data;
	       
	        if (typeof $scope.Graph == 'undefined'){

	        	$scope.Graph = [0,0,0,0];
	         }
		 $scope.data = $scope.Graph;	
   		 $scope.labels = ["No Sales","No End Of Day" , "Failed", "Done"];
  		 $scope.series = ['Done', 'Failed', 'No End Of Sales','No Sales'];    
	    	
	    });	
  
  }

});

