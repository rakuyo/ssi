'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:Poll05Poll05Ctrl
 * @description
 * # Poll05Poll05Ctrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('Poll05Poll05Ctrl', function ($scope, CRUD) {
   $scope.poll05s = function(){
		CRUD.get('/poll05s').then(function(response)	{
		 $scope.poll05s = response.data;
			console.log ($scope.poll05s);
		});	
	}
  });
