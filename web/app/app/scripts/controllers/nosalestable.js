'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:NosalestableCtrl
 * @description
 * # NosalestableCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('NosalestableCtrl', function ($scope,CRUD,$rootScope) {
    $scope.NoSalesTable= function() {
	 CRUD.get('/NoSales/'+$rootScope.date).then(function(response)  {
        $scope.NoSalesTable = response.data;
     	 console.log($scope.NoSalesTable);	 
     });
	}
  });
