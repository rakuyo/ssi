'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:NoendofsalesCtrl
 * @description
 * # NoendofsalesCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('NoendofsalesCtrl', function ($scope,$rootScope,CRUD) {
    $scope.NoEndOfSales= function() {
	 CRUD.get('/NoEndofSales/'+$rootScope.date).then(function(response)  {
        $scope.NoEndOfSales = response.data;
     	 console.log($scope.NoEndOfSales);	 
     });
	}

  });
