'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:TabledoneCtrl
 * @description
 * # TabledoneCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('TabledoneCtrl', function ($scope,CRUD,$rootScope) {
    console.log($rootScope.date);


$scope.TableDone= function() {
	 CRUD.get('/Done/'+$rootScope.date).then(function(response)  {
        $scope.TableDone = response.data;
     	 console.log($scope.TableDone);	 
     });

	}

  });
