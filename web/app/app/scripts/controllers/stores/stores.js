'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:StoresStoresCtrl
 * @description
 * # StoresStoresCtrl
 * Controller of the appApp
 */
angular.module('appApp')
  .controller('StoresStoresCtrl', function ($scope,CRUD,$route) {
  $scope.stores = function(){
		CRUD.get('/stores').then(function(response)	{
		 $scope.stores = response.data;
		
		});	
	}
 $scope.AddStore = function(){ 	
	
// generates select data
	CRUD.get('/store_names').then(function(response){
		 $scope.StoreSelect = response.data;
		});		 
	CRUD.get('/brand_names').then(function(response){
		 $scope.BrandSelect = response.data;
			
		});		 
	CRUD.get('/jda_names').then(function(response){
		 $scope.JdaSelect = response.data;
			
		});		 

	
	}
	
//Add Store
	$scope.SaveStore = function(StoreId,BrandId,JdaID){ 	
	

	console.log(StoreId);

	var obj = {
		'StoreId':StoreId,
		'BrandId':BrandId,
		'JdaID':JdaID,
		'JdaPath':$scope.JdaPath,
		'Code':$scope.Code,
		'IP':$scope.IP,
		'Opening':$scope.Opening,
		'Closing':$scope.Closing,
		'Assigned':$scope.Assigned
	};
	var params = $.param(obj);
	CRUD.post('/AddStore',params).then(function(response){      	
		alert("New Data Added");
		
	});


}

// Active/Deactivate
	$scope.Activate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/ActivateStore/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}
	$scope.Deactivate = function(id){
		$scope.index = id;
		console.log ($scope.index);	
		CRUD.put('/DeactivateStore/'+$scope.index).then(function(response){      	
		alert("Update Success");
		$route.reload();
		});
	}




});
