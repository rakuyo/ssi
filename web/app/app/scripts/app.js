'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # appApp
 *
 * Main module of the application.
 */
angular
  .module('appApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'chart.js'

  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/test', {
        templateUrl: 'views/test/index.html',
        controller: 'TestIndexCtrl'
      })
      .when('/poll05/poll05', {
        templateUrl: 'views/poll05/poll05.html',
        controller: 'Poll05Poll05Ctrl'
      })
      .when('/brands/brands_table', {
        templateUrl: 'views/brands/brands_table.html',
        controller: 'BrandsBrandsTableCtrl'
      })
      .when('/eod_uploads/eod_uploads_table', {
        templateUrl: 'views/eod_uploads/eod_uploads_table.html',
        controller: 'EodUploadsEodUploadsTableCtrl'
      })
      .when('/eod_uploads/eod_uploads', {
        templateUrl: 'views/eod_uploads/eod_uploads.html',
        controller: 'EodUploadsEodUploadsCtrl'
      })
      .when('/stores/stores', {
        templateUrl: 'views/stores/stores.html',
        controller: 'StoresStoresCtrl'
      })
      .when('/jda/jda', {
        templateUrl: 'views/jda/jda.html',
        controller: 'JdaJdaCtrl'
      })
      .when('/location/location', {
        templateUrl: 'views/location/location.html',
        controller: 'LocationLocationCtrl'
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/TableDone', {
        templateUrl: 'views/tabledone.html',
        controller: 'TabledoneCtrl'
      })
      .when('/FailedTable', {
        templateUrl: 'views/failedtable.html',
        controller: 'FailedtableCtrl'
      })
      .when('/NoSalesTable', {
        templateUrl: 'views/nosalestable.html',
        controller: 'NosalestableCtrl'
      })
      .when('/NoEndOfSales', {
        templateUrl: 'views/noendofsales.html',
        controller: 'NoendofsalesCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
