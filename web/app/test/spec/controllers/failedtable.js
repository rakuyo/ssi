'use strict';

describe('Controller: FailedtableCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var FailedtableCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FailedtableCtrl = $controller('FailedtableCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
