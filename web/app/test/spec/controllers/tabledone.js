'use strict';

describe('Controller: TabledoneCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var TabledoneCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TabledoneCtrl = $controller('TabledoneCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
