'use strict';

describe('Controller: Poll05Poll05Ctrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var Poll05Poll05Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Poll05Poll05Ctrl = $controller('Poll05Poll05Ctrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
