'use strict';

describe('Controller: NoendofsalesCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var NoendofsalesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NoendofsalesCtrl = $controller('NoendofsalesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
