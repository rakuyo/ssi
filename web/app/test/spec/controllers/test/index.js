'use strict';

describe('Controller: TestIndexCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var TestIndexCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TestIndexCtrl = $controller('TestIndexCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
