'use strict';

describe('Controller: JdaJdaCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var JdaJdaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    JdaJdaCtrl = $controller('JdaJdaCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
