'use strict';

describe('Controller: EodUploadsEodUploadsTableCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var EodUploadsEodUploadsTableCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EodUploadsEodUploadsTableCtrl = $controller('EodUploadsEodUploadsTableCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
